import style from './styles/index.scss';
import React from 'react';
import axios from 'axios';
import Slider from 'react-slick';
import NumberFormat from 'react-number-format';
import Spinner from 'react-spinkit';

var NextArrow = React.createClass({
    render: function() {
        return <div {...this.props} className="picker-next icon-chevron-thin-right"></div>;
    }
});

var PrevArrow = React.createClass({
    render: function() {
        return <div {...this.props} className="picker-prev icon-chevron-thin-left"></div>;
    }
});

export default class App extends React.Component {
    constructor(props) {
     super(props)
     this.state = {
       listings: []
     }
   }

  componentWillMount() {
      const API_URL = 'https://www.thelistinghub.com.au/listing/picker?position=' + this.props.position
      const script = document.createElement("script")
      script.src = "https://use.typekit.net/hnt4smn.js"
      script.async = true;
      document.body.appendChild(script)
        var _this = this;
        this.serverRequest =
          axios
            .get(API_URL)
            .then(function(result) {
              _this.setState({
                listings: result.data
              })
            })
      }

  componentWillUnmount() {
    this.serverRequest.abort();
  }

  render() {
      let {listings} = this.state
      let picker_slides = ''
      let pickerLuxuryHeader = ''
      let pickerBrandedHeader = ''
      let sliderSettings = {
          dots: false,
          infinite: true,
          swipeToSlide: true,
          speed: 500,
          arrows: true,
          nextArrow: <NextArrow />,
          prevArrow: <PrevArrow />,
          draggable: false,
          fade: false,
          lazyLoad: true,
          autoplay: false,
          autoplaySpeed: 5000,
          slidesToShow: 1,
          slidesToScroll: 1,
          adaptiveHeight: false,
      }
      for (let listing of listings) {
          if (listing.branded === false) {
              pickerLuxuryHeader = (
                  <h1 className="luxuryHeading">
                      <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" style={{ maxWidth: 180 }} viewBox="0 0 163.34 25.92">
                          <title>Luxury Picker Logo</title>
                          <g>
                              <path d="M15.64,20.17H.69v-.23H3.22V2H.69V1.77H9.43V2H6.9V19.94h2A6.25,6.25,0,0,0,13.51,18a6.28,6.28,0,0,0,1.9-4.61h.23Z" transform="translate(-0.69 -0.8)"/>
                              <path d="M23.74,20.4q-5,0-5-5.34V7.56H16.56V7.36h5.52v8.83q0,.94.06,1.54a5.94,5.94,0,0,0,.25,1.22,1.67,1.67,0,0,0,.63.92,2,2,0,0,0,1.15.3,2.57,2.57,0,0,0,2.09-1.09,5.15,5.15,0,0,0,.85-3.21V7.56H24.93V7.36h5.52V20h2.19v.21H27.12V18.9A4.77,4.77,0,0,1,23.74,20.4Z" transform="translate(-0.69 -0.8)"/>
                              <path d="M44.14,7.56H41.7V7.36h4.69v.21h-2L40.85,12l5.31,8h2v.21H39.26V20h2.88l-3.5-5.22L34.43,20h2.88v.21H31.95V20h2.19l4.37-5.43-4.65-7H31.95V7.36h8.37v.21H37.88l2.83,4.25Z" transform="translate(-0.69 -0.8)"/>
                              <path d="M53.93,20.4q-5,0-5-5.34V7.56H46.76V7.36h5.52v8.83q0,.94.06,1.54a5.94,5.94,0,0,0,.25,1.22,1.67,1.67,0,0,0,.63.92,2,2,0,0,0,1.15.3,2.57,2.57,0,0,0,2.09-1.09,5.15,5.15,0,0,0,.85-3.21V7.56H55.13V7.36h5.52V20h2.19v.21H57.32V18.9A4.77,4.77,0,0,1,53.93,20.4Z" transform="translate(-0.69 -0.8)"/>
                              <path d="M68.82,11.68V20h2.64v.21H63.3V20h2.19V7.56H63.3V7.36h5.52V8.62a6.44,6.44,0,0,1,1.44-1.13A3.68,3.68,0,0,1,72,7.13,3.82,3.82,0,0,1,74.44,8a3.16,3.16,0,0,1,1.12,2.63A2.35,2.35,0,0,1,75,12.29a2,2,0,0,1-1.59.63,2,2,0,0,1-1.3-.47,1.63,1.63,0,0,1-.56-1.32A1.78,1.78,0,0,1,72.68,9.4c.52-.23.78-.51.78-.85A1,1,0,0,0,73,7.68a2.24,2.24,0,0,0-1.26-.32,2.51,2.51,0,0,0-2.07,1.08A5.29,5.29,0,0,0,68.82,11.68Z" transform="translate(-0.69 -0.8)"/>
                              <path d="M83,20.35,77,7.56H74.79V7.36h8.79v.21H80.71l4.07,8.72,3.86-8.72H86V7.36h5.13v.21H88.92L81.3,24.63a3,3,0,0,1-2.9,2.09,3.39,3.39,0,0,1-2.28-.85,2.83,2.83,0,0,1-1-2.25,2,2,0,0,1,.52-1.38A1.75,1.75,0,0,1,77,21.66a1.71,1.71,0,0,1,1.15.41,1.49,1.49,0,0,1,.48,1.2,1.53,1.53,0,0,1-.3.93,1.58,1.58,0,0,1-.76.56q-.6.25-.6.74a.84.84,0,0,0,.41.76,1.91,1.91,0,0,0,1,.25,2.72,2.72,0,0,0,2.62-1.86Z" transform="translate(-0.69 -0.8)"/>
                              <path d="M99.5,19.94V2H97V1.77h9.29a6.22,6.22,0,0,1,4.63,1.74,5.5,5.5,0,0,1,1.71,4,5.15,5.15,0,0,1-1.7,3.84,6.43,6.43,0,0,1-4.65,1.63h-3.08v7h2.53v.23H97v-.23ZM103.18,2V12.71h2a3.06,3.06,0,0,0,2.79-1.14,8.22,8.22,0,0,0,.72-4.11,8.83,8.83,0,0,0-.74-4.23A3,3,0,0,0,105.13,2Z" transform="translate(-0.69 -0.8)"/>
                              <path d="M114,7.36h5.52V20h2.18v.21H114V20h2.18V7.56H114Zm5.21-5.27a1.8,1.8,0,0,1,.56,1.33,1.84,1.84,0,0,1-.56,1.35,1.9,1.9,0,0,1-3.25-1.35,1.82,1.82,0,0,1,.55-1.33,1.93,1.93,0,0,1,2.7,0Z" transform="translate(-0.69 -0.8)"/>
                              <path d="M134.62,16.1h.32A5.8,5.8,0,0,1,133,19.22a5.42,5.42,0,0,1-3.56,1.17,6.46,6.46,0,0,1-4.69-1.86,6.36,6.36,0,0,1-1.91-4.76,6.47,6.47,0,0,1,6.6-6.65,5.93,5.93,0,0,1,3.74,1.22,3.66,3.66,0,0,1,1.58,3,2,2,0,0,1-.58,1.44,1.86,1.86,0,0,1-1.38.59,1.89,1.89,0,0,1-1.91-1.91,1.94,1.94,0,0,1,.94-1.7l.17-.1.21-.15.16-.14a.61.61,0,0,0,.15-.18.45.45,0,0,0,0-.21q0-.67-.9-1.14a4.75,4.75,0,0,0-2.23-.47,3.69,3.69,0,0,0-1.58.3,1.94,1.94,0,0,0-.93.89,4.16,4.16,0,0,0-.4,1.22,9.73,9.73,0,0,0-.1,1.53v4.9a9.73,9.73,0,0,0,.1,1.53,4.3,4.3,0,0,0,.4,1.23,2,2,0,0,0,.93.91,3.56,3.56,0,0,0,1.58.31,5.12,5.12,0,0,0,3.28-1.08A5.58,5.58,0,0,0,134.62,16.1Z" transform="translate(-0.69 -0.8)"/>
                              <path d="M143.43,13.57l-1.61,1.56V20h2v.21h-7.45V20h2.18V1h-2.18V.8h5.5v14l7.41-7.22h-3.61V7.36h6.55v.21h-2.58L146,11.13,151.82,20H154v.21h-8.35V20h2.09Z" transform="translate(-0.69 -0.8)"/>
                              <path d="M158.38,7.36a2.42,2.42,0,0,0-1.6.51,1.68,1.68,0,0,0,.2,2.73,7.42,7.42,0,0,0,2,.94q1.16.38,2.32.85a5,5,0,0,1,2,1.44,3.61,3.61,0,0,1,.8,2.39,3.75,3.75,0,0,1-1.43,3,5.64,5.64,0,0,1-3.75,1.2,6.4,6.4,0,0,1-2.23-.5,6.06,6.06,0,0,0-1.91-.49q-.81,0-1,1h-.25V14.14h.25a10.6,10.6,0,0,0,1.89,4.16,4,4,0,0,0,3.29,1.84,2.87,2.87,0,0,0,1.85-.6,2.12,2.12,0,0,0,.32-2.89,3.18,3.18,0,0,0-1.09-.87,13.21,13.21,0,0,0-1.47-.62q-.8-.29-1.62-.63a9.34,9.34,0,0,1-1.48-.78,3.36,3.36,0,0,1-1.09-1.2,3.52,3.52,0,0,1-.42-1.75,3.36,3.36,0,0,1,1.21-2.61,4.63,4.63,0,0,1,3.19-1.07,7.71,7.71,0,0,1,2,.32,7.07,7.07,0,0,0,1.72.32.91.91,0,0,0,.94-.64h.23v5H163a6.41,6.41,0,0,0-1.6-3.23A3.92,3.92,0,0,0,158.38,7.36Z" transform="translate(-0.69 -0.8)"/>
                          </g>
                      </svg>
                  </h1>
              )
          }
      }
      picker_slides = listings.map((listing, i) =>
      <div key={i}>
        {listing.branded
          ?
          <h1 className="brandedHeading" style={{ backgroundColor: listing.agency_background_colour }}>
              <a href={listing.agency_url + '?picker=' + this.props.position}
                 target="_blank"
                 title={listing.agency_name}>
                  <img
                      src={listing.agency_logo}
                      alt={listing.agency_name}
                      className={style.agencyLogo}
                  />
              </a>
          </h1>
          : ''
        }
          <div id={listing.branded ? "brandedPickerCard" : "luxuryPickerCard"}>
            <div className={style.imageWrapper}>
                <a href={listing.readable_url + '?picker=' + this.props.position}
                   target="_blank"
                   title={listing.headline}>
                    <img className={style.image}
                     src={listing.main_image}
                     alt={listing.headline}
                     width="100%"
                     height="auto"
                   />
                </a>
            </div>
            <div className={style.details}>
                <div className={style.information}>
                    <div className={style.price}><NumberFormat value={listing.price} displayType={'text'} thousandSeparator={true} prefix={'$'} /></div>
                    <address>
                        {listing.display_address}
                    </address>
                </div>
                <div className="">
                    <ul className={style.features}>
                        <li>{listing.residential_listing['bedroom']} Beds <span className={style.border}>|</span></li>
                        <li>{listing.residential_listing['bathrooms']} Baths <span className={style.border}>|</span></li>
                        <li>{listing.residential_listing['cars']} Cars <span className={style.border}>|</span></li>
                        <li>{listing.asset_sub_class}</li>
                    </ul>
                </div>
                <div className={style.footer}>
                    <span className={style.agentLogo}>
                        <a href={listing.agency_url + '?picker=' + this.props.position}
                           target="_blank"
                           title={listing.agency_name}>
                             {listing.branded
                               ?
                               <span>{listing.agency_name}</span>
                               :
                               <img
                                   src={listing.agency_logo}
                                   alt={listing.agency_name}
                                   className={style.agencyLogo}
                               />
                             }
                        </a>
                    </span>
                    <span className={style.detailsBtn}>
                      {listing.branded
                        ?
                        <a className={style.btn}
                           style={{ backgroundColor: listing.agency_background_colour, color: listing.agency_foreground_colour }}
                           href={listing.readable_url + '?picker=' + this.props.position}
                           title="See more information"
                           target="_blank">
                            DETAILS
                        </a>
                        :
                        <a className={style.btn}
                           href={listing.readable_url + '?picker=' + this.props.position}
                           title="See more information"
                           target="_blank">
                            DETAILS
                        </a>
                      }
                    </span>
                </div>
            </div>
          </div>
      </div>
    )
    return (
      <div className={style.pickerContainer}>
          {pickerLuxuryHeader}
          {picker_slides.length > 0
             ?
             <Slider {...sliderSettings}>{picker_slides}</Slider>
             :
             <Spinner
               spinnerName='double-bounce'
               className={style.loading}
             />
         }
      </div>
    )
  }
}
