import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import App from './app.jsx';

for (let pickerPosition of pickerPositions) {
    render( <AppContainer><App position={pickerPosition.position}/></AppContainer>, document.querySelector(pickerPosition.id));
}

if (module && module.hot) {
  module.hot.accept('./app.jsx', () => {
    const App = require('./app.jsx').default;
      for (let pickerPosition of pickerPositions) {
          render( <AppContainer><App position={pickerPosition.position}/></AppContainer>, document.querySelector(pickerPosition.id));
      }
  });
}
